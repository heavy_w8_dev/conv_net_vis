from setuptools import setup, find_packages

setup(
    name='conv_net_vis',
    version='0.1.0',
    description='Pytorch based vis library based on work of Francesco Saverio Zuppichini',
    author='Uladzislau Lazouski, Eugene Olkhovik',
    author_email='uladzislau.lazouski@gmail.com',
    url='https://gitlab.com/heavy_w8_dev/conv_net_vis',
    packages=find_packages(),
    provides=['conv_net_vis'],
    license='MIT',
    install_requires=[
        'numpy',
        'opencv-python',
        'Pillow',
        'torch',
        'torchvision',
        'matplotlib',
    ],
    classifiers=[
        'Programming Language :: Python :: 3',
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Scientific/Engineering :: Artificial Intelligence',
        'Operating System :: OS Independent',
    ],
)
